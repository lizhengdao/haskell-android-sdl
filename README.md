# Build environment for Haskell apps on Android using SDL and optionally Cairo.

## To use it, see the [sdl-gles-cairo-demo](https://gitlab.com/alleycatcc/sdl-gles-cairo-demo) project.

## If you want to try to build/tweak the Docker image yourself (takes a *long* time):

    git submodule update --init --recursive
    bin/docker
    # --- edit Dockerfile?
    bin/docker

## To build the project yourself (warning: takes a *long* time and is very challenging):

    git submodule update --init --recursive
    cp vars.sh-example vars.sh
    cp vars-local.sh-example vars-local.sh
    cp vars-plugin.sh-example vars-plugin.sh
    # --- edit the values in all of the above.
    bin/build-project