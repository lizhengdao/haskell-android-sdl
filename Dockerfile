FROM alleycatcc/ghc-armv7-linux-androideabi:latest

# --- needed for legacy android-ndk-r8c
RUN dpkg --add-architecture i386

RUN apt-get update
RUN apt-get -y install openjdk-8-jdk mercurial ant imagemagick

# --- needed for legacy android-ndk-r8c
RUN apt-get -y install libc6:i386 zlib1g:i386 libstdc++6:i386

# --- to trick cairo's configure script
RUN apt-get -y install libpng-dev libpixman-1-dev

# --- @todo do this outside the Dockerfile and then COPY.
RUN git clone https://gitlab.com/alleycatcc/haskell-android-sdl && \
    cd haskell-android-sdl && \
    git reset --hard 925434d2dd1f092edb4c50d624345e145e35c013

RUN cd haskell-android-sdl && git submodule update --init --recursive
RUN cd haskell-android-sdl && cp vars-local.sh-docker vars-local.sh
RUN cd haskell-android-sdl && cp vars-user.sh-example vars-user.sh

RUN cd haskell-android-sdl && bin/build-project clean
RUN cd haskell-android-sdl && bin/build-project init-gpg
RUN cd haskell-android-sdl && bin/build-project get-sdk
RUN cd haskell-android-sdl && bin/build-project init-extra-sources

# --- @todo remove these legacy versions
COPY android-ndk-r8c.tar /
RUN cd /android/android-ndk && tar xvf /android-ndk-r8c.tar
COPY android-sdk-api-17-tools-r25.2.5-platform-tools-r25.0.5.tar /
RUN cd /android && tar xvf /android-sdk-api-17-tools-r25.2.5-platform-tools-r25.0.5.tar

RUN cd haskell-android-sdl && bin/build-project build-extra-sources
RUN cd haskell-android-sdl && bin/build-project prepare-build-pre-hs
RUN cd haskell-android-sdl && bin/build-project hs-build-deps-init
RUN cd haskell-android-sdl && bin/build-project hs-build-deps
