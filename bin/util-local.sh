set -eu
set -o pipefail

gpg-cmd () {
    fun acat-gpg-cmd "$gpgdir" "$@"
}

gpg-verify () {
    fun gpg-cmd --verify "$@"
}

awk1 () {
    cmd awk '{print $1}'
}

get-sha256 () {
    local ret="$1"
    local file="$2"
    fun pipe-capture _ret0 awk1 sha256sum "$file"
    local sha=$_ret0
    retvar "$ret" "$sha"
}
