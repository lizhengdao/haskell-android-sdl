set -eu
set -o pipefail

# builtlibsdir="$rootdir"/built-libs

bindir=$(realpath "$(dirname "$0")")
rootdir="$bindir"/..
srcdir="$rootdir"/src
hsdir="$rootdir"/hs-lib
gpgdir="$rootdir"/gpg
patchdir="$rootdir"/patches
sdlbuilddir="$rootdir"/build-sdl

sdlbuilddirsources_parts=("$sdlbuilddir" sources)
sdlbuilddirsources=$(join-out / sdlbuilddirsources_parts)

sandboxdir_parts=("$hsdir" .cabal-sandbox)
sandboxdir=$(join-out / sandboxdir_parts)

builddir_parts=("$rootdir" build)
hsdirbuild_parts=("$hsdir" build)

builddir=$(join-out / builddir_parts)
hsdirbuild=$(join-out / hsdirbuild_parts)

sdlbuilddirbuild_parts=("$sdlbuilddir" build)
sdlbuilddirbuild=$(join-out / sdlbuilddirbuild_parts)

ardir_parts=("$hsdir" ar)
ardir=$(join-out / ardir_parts)

builtlibsdirsource="$sdlbuilddir"/build/libs

jnidir="$builddir"/jni

targethost=armv7-linux-androideabi

ghcarmeabiv7a=armv7-linux-androideabi-ghc
ghcpkgarmeabiv7a=armv7-linux-androideabi-ghc-pkg
clangarmeabiv7a=armv7-linux-androideabi-clang
ldarmeabiv7a=armv7-linux-androideabi-ld
striparmeabiv7a=armv7-linux-androideabi-strip

ghcarmeabiv7a_numericversion=$("$ghcpath"/"$ghcarmeabiv7a" --numeric-version)

packageconfdparts=("$hsdir" .cabal-sandbox arm-linux-android-ghc-"$ghcarmeabiv7a_numericversion"-packages.conf.d)
packageconfd=$(join-out / packageconfdparts)

gpgkeys=(
    F54984BFA16C640F
)

# --- 14 is the latest one we can generate using r8c
androidtoolsapiversion=14
# --- and the sdk is initted to use 17.
androidsdkapiversion=17

# android-r8: no toolchain necessary -- will be used for ndk build.
# android-r16b: for gcc / linker toolchain.

sourcesdir_parts=("$rootdir" sources)
sourcesdir=$(join-out / sourcesdir_parts)

cmain=main.c
srcfiles=("$srcdir"/"$cmain")
buildhsdirarmeabiv7a="$hsdir"/build/armeabi-v7a

# sdldir="$sourcesdir"/SDL
sdldir="$sdlbuilddirsources"/SDL
sdlimagedir="$sourcesdir"/SDL_image
sdlttfdir="$sourcesdir"/SDL_ttf

target_arm=armeabi-v7a
target_x86=x86

sdlimagerevision=543:c28eb37178a1

# --- this is the project which provides the android build system,
# originally from sdl's website.
# --- we git pull it but don't treat it as a submodule.
sdlandroidbuildstub=sdl-android-build
sdlandroidbuilddir="$sourcesdir"/"$sdlandroidbuildstub"
sdlandroidbuildurl=https://gitlab.com/alleycatcc/"$sdlandroidbuildstub"
sdlandroidbuildrevision=6205b938a83c1af30c56c10e5363fe9c22836ea7

sdlremote=http://hg.libsdl.org/SDL
sdlremotestub=SDL
sdlimageremote=http://hg.libsdl.org/SDL_image
sdlimageremotestub=SDL_image
sdlrevision=11646:c875be618b90
sdlttfremote=http://hg.libsdl.org/SDL_ttf/
sdlttfremotestub=SDL_ttf
sdlttfrevision=317:a196a5dc11b6

# --- we use version 8 for ...
#    building sdl libs
#    the final build
#    @todo better name
ndkdir="$ndkrootdir"/android-ndk-r8c
# --- we use version 16 for everything else.
#   cabal, final hs lib, extra sources.
# --- note: not necessarily the same ndk version as ndkdir.
crossbindir="$crossbinrootdir"/bin

png16remote=https://sourceforge.net/projects/libpng/files/libpng16/1.6.34/libpng-1.6.34.tar.xz/download
png16ascremote=https://sourceforge.net/projects/libpng/files/libpng16/1.6.34/libpng-1.6.34.tar.xz.asc
png16dirstub=libpng-1.6.34
png16dir="$sourcesdir"/"$png16dirstub"

pixmanremote=git://anongit.freedesktop.org/git/pixman.git
pixmanrevision=pixman-0.34.0
pixmandirstub=pixman
pixmandir="$sourcesdir"/"$pixmandirstub"

cairoremote=git://anongit.freedesktop.org/git/cairo
cairomodver=1.14.8
cairorevision="$cairomodver"
cairodirstub=cairo
cairodir="$sourcesdir"/"$cairodirstub"

# --- we take glu and glu dev from the debian archives and *only* use them
# for compiling and linking the necessary haskell modules (OpenGLRaw in
# particular).
# --- xxx how do we know the exact version we need?
# --- it is unknown whether bundling the library would make the glu calls
# possible -- probably the debian library is not suited for running as-is on
# the android.
# --- glu calls in the code should be avoided or provided some other way.
# --- there is a project by Mike Gorchak on Google Code which is supposed to
# be a GLU GLES port but last update is 2009.
# --- Android Java does have some glu functions, including gluSphere &
# gluProject. Call through JNI?

glulibremote=http://ftp.de.debian.org/debian/pool/main/libg/libglu/libglu1-mesa_9.0.0-2.1_armel.deb
glulibremotesha256=939a07f68b6a2094591326af59c86391b39b2813a7f10c072ce08969acc3d4a9
gludevremote=http://ftp.de.debian.org/debian/pool/main/libg/libglu/libglu1-mesa-dev_9.0.0-2.1_armel.deb
gludevremotesha256=2c3ccfcb5a90bab07cb0815d47f5b53ef9f3cd7c5a220c865a5d6df8294d0573
glulibdir="$sourcesdir"/glu/usr/lib/arm-linux-gnueabi
gluincludedir="$sourcesdir"/glu/usr/include

ndktoolchainx86dir="$ndkrootdir"/toolchain-r8c-x86-"$androidtoolsapiversion"
ndktoolchainarmdir="$ndkrootdir"/toolchain-r8c-armeabi-v7a-"$androidtoolsapiversion"
ndkbuildcmd="$ndkdir"/ndk-build

sdktoolsdir="$sdkdir"/tools
sdkplattoolsdir="$sdkdir"/platform-tools
sdkplatformtoolsdir="$sdkdir"/platform-tools
# --- we use an old sdk on purpose because a lot of things have been
# deprecated (ant/tools folders, `android project update` command)
sdktoolsurl=https://dl.google.com/android/repository/tools_r25.2.5-linux.zip
sdkplatformtoolsurl=https://dl.google.com/android/repository/platform-tools_r25.0.5-linux.zip

buildtoolsversion=25.0.2

# --- for bypassing the license prompt on automated installs.
# --- leave blank to skip.
# --- to determine this string, do a manual install and check the
# `licenses/` folder.
sdklicensestring=$'\n
d56f5187479451eabf01fb78af6dfcb131a6481e'

read cabalopts <<EOT
    --builddir="$builddir" \
    --with-ghc="$ghcarmeabiv7a" \
    --ghc-options=-fPIC \
    --with-ghc-pkg="$ghcpkgarmeabiv7a" \
    --with-gcc="$clangarmeabiv7a" \
    --gcc-option=-fPIC \
    --with-strip="$striparmeabiv7a" \
    --with-ld="$ldarmeabiv7a" \
    --hsc2hs-options='--cross-compile -cflag=-fPIC'
EOT

cairolibarmsoftremote=http://ftp.de.debian.org/debian/pool/main/c/cairo/libcairo2_1.14.8-1_armel.deb
cairolibdevarmsoftremote=http://ftp.de.debian.org/debian/pool/main/c/cairo/libcairo2-dev_1.14.8-1_armel.deb

pkgconfigreal=$(which pkg-config || error "no pkg-config found")

# xxx arrays for these
srcassetsdir="$rootdir"/assets
buildassetsdir="$builddir"/assets

tmptmpdirparts=("$tmpdir" haskell-android-sdl)

haskellpackages_deps=()
haskellpackages_deps_names=()

haskellpackages_plugin=()
haskellpackages_plugin_names=()

# xxx repetition

get-deps-deps () {
    local type
    local pack
    local meta
    while read type pack meta; do
        if [ -z "$pack" ]; then
            continue
        fi
        if [[ "$type" =~ ^[#] ]]; then
            continue
        fi
        haskellpackages_deps+=("$type $pack")
        if [ ! "$meta" = noarchive ]; then
            haskellpackages_deps_names+=("$(basename "$pack")")
        fi
    done <<< "$cabaldepscross"
}

get-deps-plugin () {
    local type
    local pack
    local meta
    while read type pack meta; do
        if [ -z "$pack" ]; then
            continue
        fi
        if [[ "$type" =~ ^[#] ]]; then
            continue
        fi
        haskellpackages_plugin+=("$type $pack")
        if [ ! "$meta" = noarchive ]; then
            haskellpackages_plugin_names+=("$(basename "$pack")")
        fi
    done <<< "$acat_plugin_haskellpackagesuser"
}

fun get-deps-deps

if [ -n "${acat_plugin_haskellpackagesuser:-}" ]; then
    fun get-deps-plugin
fi
