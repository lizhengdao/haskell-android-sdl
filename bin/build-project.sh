#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")
rootdir="$bindir"/..

. "$bindir"/functions.bash
. "$bindir"/util.sh
. "$bindir"/util-local.sh

USAGE="Usage: $0 { mode [mode...] } where mode is one of

    clean
    init-gpg
    get-sdk
    init-extra-sources
    build-extra-sources
    prepare-build-pre-hs
    hs-build-deps-init
    hs-build-deps
    hs-build-user
    prepare-build
    main-build
    apk
"

while getopts h-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

# xxx would be good to use a flag to tell it where to find this; strange to
# copy it into root dir.
if [ -e "$rootdir"/vars-plugin.sh ]; then
    . "$rootdir"/vars-plugin.sh
fi

. "$rootdir"/vars-local.sh
. "$rootdir"/vars-user.sh
. "$bindir"/vars-build.sh

# --- we assume that the ndk and toolchains have been set up during the
# cross-compilation stage, as well as clang.
# --- if necessary we could provide those functions as a util.

clean () {
    fun safe-rm-dir-array-allow-absolute builddir_parts
    fun safe-rm-dir-array-allow-absolute sdlbuilddirbuild_parts
    fun safe-rm-dir-array-allow-absolute sdlbuilddirsources_parts
    fun safe-rm-dir-array-allow-absolute ardir_parts
    fun safe-rm-dir-array-allow-absolute hsdirbuild_parts
    fun safe-rm-dir-array-allow-absolute sandboxdir_parts
    fun safe-rm-dir-array-allow-absolute sourcesdir_parts
    cmd rm -f "$hsdir"/cabal.sandbox.config
}

init-gpg () {
    cmd chmod 700 "$gpgdir"
    chd "$gpgdir"
    local i
    for i in "${gpgkeys[@]}"; do
        fun gpg-cmd --import "$i".pub
    done
    chd-back
}

# --- update 2020-03: we copy the sdk manually because the newer sdk refuses
# to work with some of the old versions we are using.

get-sdk () {
    true
}

get-sdk1 () {
    mci
    mcb acat-get-android-sdk
    mcb   "$sdkdir"
    mcb   "$sdktoolsurl"
    mcb   "$javahome"
    mcb   "$androidsdkapiversion"
    mcb   "$buildtoolsversion"
    mcb   "$sdkplatformtoolsurl"
    mcb   "$sdklicensestring"
    mcg
}

# --- android ndk r8c, which we are using for legacy reasons to run the sdl
# build script, is no longer available and *must* be copied manually.

get-ndk1 () {
    local stub=android-ndk-r8c
    local ndkremote=https://dl.google.com/android/repository/"$stub"-linux-x86_64.zip
    # xxx vars
    mci
    mcb acat-get-android-ndk
    mcb   "$ndkrootdir"
    mcb   "$stub"
    mcb   "$ndkremote"
}

get-ndk () {
    true
    # --- r8c
    # fun get-ndk1
    # --- r16b.
    # fun get-ndk2
}

init-sources-dir () {
    fun safe-rm-dir-array-allow-absolute sourcesdir_parts
    mkd "$sourcesdir"
}

get-sdl-android-build () {
    cmd git clone "$sdlandroidbuildurl" "$sdlandroidbuildstub"
    chd "$sdlandroidbuildstub"
    cmd git submodule update --init --recursive
    cmd git reset --hard "$sdlandroidbuildrevision"
    cmd rm -rf .git
}

get-sdl-android-build-main () {
    chd "$sourcesdir"
    fun get-sdl-android-build
}

get-sdl-android-build-sub () {
    chd "$sdlbuilddirsources"
    fun get-sdl-android-build
}

# ------ @todo figure out the includes
# ------ @todo remove Android.mk so they don't build.

get-sdl () {
    fun safe-rm-dir-allow-absolute "$sdlbuilddirsources" "$sdlremotestub"
    chd "$sdlbuilddirsources"
    cmd hg clone "$sdlremote"
    chd "$sdlremotestub"
    cmd hg revert -r "$sdlrevision" --all

    # cmd rm -f Android.mk
}

get-sdl-image () {
    fun safe-rm-dir-allow-absolute "$sdlbuilddirsources" "$sdlimageremotestub"
    chd "$sdlbuilddirsources"
    cmd hg clone "$sdlimageremote"
    chd "$sdlimageremotestub"
    cmd hg revert -r "$sdlimagerevision" --all
    # cmd rm -f Android.mk
}

get-sdl-ttf () {
    fun safe-rm-dir-allow-absolute "$sdlbuilddirsources" "$sdlttfremotestub"
    chd "$sdlbuilddirsources"
    cmd hg clone "$sdlttfremote"
    chd "$sdlttfremotestub"
    cmd hg revert -r "$sdlttfrevision" --all

    # cmd rm -f Android.mk
}

# ------ /sdl includes

sdl-build-prepare () {

    stack-push-path

    xport-prepend PATH "$sdkplatformtoolsdir"
    xport-prepend PATH "$sdktoolsdir"
    xport-prepend PATH "$ndkdir"

    fun safe-rm-dir-allow-absolute "$sdlbuilddir" build

    mci
    mcb "$sdlbuilddirsources"/sdl-android-build/androidbuild.sh
    mcb   "$sdlbuilddir"/build
    mcb   "$sdlbuilddirsources"/"$sdlremotestub"
    mcb   "android-$androidsdkapiversion"
    mcb   dummy-app-name
    mcb   "$sdlbuilddir"/src/noop.c
    mcg

    stack-pop-path
}

sdl-build-prepare-build-sources () {
    # --- not necessary to link SDL: it was already done by the script
    # (prepare-project-dir ())

    local jnidir="$sdlbuilddir"/build/jni
    cmd ln -s "$sdlbuilddirsources"/"$sdlimageremotestub" "$jnidir"/SDL2_image
    cmd ln -s "$sdlbuilddirsources"/"$sdlttfremotestub" "$jnidir"/SDL2_ttf
}

android-mk-sdl () {
    cat <<-'EOF'

	LOCAL_PATH := $(call my-dir)

	include $(CLEAR_VARS)

	LOCAL_MODULE := main

	LOCAL_C_INCLUDES := $(LOCAL_PATH)/../SDL/include \
						$(LOCAL_PATH)/../SDL2_image \
						$(LOCAL_PATH)/../SDL2_ttf \

	# $(warning $(LOCAL_C_INCLUDES))

	LOCAL_SRC_FILES :=  noop.c

	LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image SDL2_ttf

	# --- should be only system libs
	LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -llog

	include $(BUILD_SHARED_LIBRARY)
	EOF
}

prepare-android-mk () {
    local jnidir=$1

    local sub
    printf -v sub 'APP_ABI := .*,APP_ABI = %s' "${targets[*]}"

    cmd sed -i s,"$sub", "$jnidir"/Application.mk

    fun redirect-out "$jnidir"/src/Android.mk cat
}

sdl-build-prepare-android-build () {
    fun redirect-in-cmd android-mk-sdl fun prepare-android-mk "$sdlbuilddir"/build/jni
}

sdl-build-build () {
    chd "$sdlbuilddir"/build
    # --- webp is not needed, and causes builds to fail.
    xport SUPPORT_WEBP false
    cmd "$ndkbuildcmd" -j
    xport SUPPORT_WEBP
}

prepare-glu () {
    mkchd "$sourcesdir"/glu

    cmd curl -Lo libglu.deb "$glulibremote"
    get-sha256 _ret0 libglu.deb
    local sha=$_ret0
    if [ ! "$sha" = "$glulibremotesha256" ]; then
        error Bad sha256
    fi
    cmd ar x libglu.deb
    cmd tar Jxvf data.tar.xz
    cmd rm -f data.tar.xz

    cmd curl -Lo libgludev.deb "$gludevremote"
    get-sha256 _ret0 libgludev.deb
    local sha=$_ret0
    if [ ! "$sha" = "$gludevremotesha256" ]; then
        error Bad sha256
    fi
    cmd ar x libgludev.deb
    cmd tar Jxvf data.tar.xz
    cmd rm -f data.tar.xz
}

# --- we pull the 'sdl-android-build' project, originally from sdl's site,
# which will provide the scripts for building the .apk.
# --- in order to install `sdl2` from cabal, we need built (cross-compiled)
# SDL2 libraries.
# --- cross-compiling the ordinary way proved difficult, so instead, we use
# a copy of the sdl-android-build system inside a subfolder to build a
# trivial c program, which also builds the sdl libs for us.
# --- also, we will need the sdl includes later.

init-extra-sources () {
    fun init-sources-dir
    fun get-sdl-android-build-main

    mkd "$sdlbuilddirsources"

    fun get-sdl
    fun get-sdl-image
    fun get-sdl-ttf
    fun get-sdl-android-build-sub
    fun prepare-glu
}

build-sdl () {
    fun sdl-build-prepare
    fun sdl-build-prepare-build-sources
    fun sdl-build-prepare-android-build
    fun sdl-build-build
}

build-png16 () {
    local file=libpng.xz
    local sigfile="$file".asc

    stack-push-path
    xport-append PATH "$crossbindir"

    stack-push-xport AR armv7-linux-androideabi-ar
    stack-push-xport AS armv7-linux-androideabi-clang
    stack-push-xport CC armv7-linux-androideabi-clang
    stack-push-xport CFLAGS -fPIC
    stack-push-xport CXX armv7-linux-androideabi-clang++
    stack-push-xport LD armv7-linux-androideabi-ld
    stack-push-xport STRIP armv7-linux-androideabi-strip

    fun safe-rm-dir-allow-absolute "$sourcesdir" "$png16dirstub"
    chd "$sourcesdir"
    cmd curl -Lo "$file" "$png16remote"
    cmd curl -Lo "$sigfile" "$png16ascremote"
    fun gpg-verify "$sigfile" "$file"
    cmd tar Jxvf "$file"
    cmd rm -f "$file" "$sigfile"
    chd libpng*

    mci
    mcb ./configure
    mcb   --host="$targethost"
    mcb   --with-sysroot="$crossbinrootdir"/sysroot
    mcg

    cmd make -j

    stack-pop AR
    stack-pop AS
    stack-pop CC
    stack-pop CFLAGS
    stack-pop CXX
    stack-pop LD
    stack-pop STRIP

    stack-pop-path
}

build-pixman () {
    stack-push-path
    xport-append PATH "$crossbindir"

    fun safe-rm-dir-allow-absolute "$sourcesdir" "$pixmandirstub"
    chd "$sourcesdir"
    cmd git clone "$pixmanremote" "$pixmandirstub"
    chd "$pixmandirstub"
    cmd git reset --hard "$pixmanrevision"

    stack-push-xport AR armv7-linux-androideabi-ar
    stack-push-xport AS armv7-linux-androideabi-clang
    stack-push-xport CC armv7-linux-androideabi-clang
    stack-push-xport CFLAGS -fPIC
    stack-push-xport CXX armv7-linux-androideabi-clang++
    stack-push-xport LD armv7-linux-androideabi-ld
    stack-push-xport STRIP armv7-linux-androideabi-strip
    stack-push-xport LDFLAGS -L"$png16dir"/.libs
    # --- don't automatically run configure after autogen.
    stack-push-xport NOCONFIGURE yes

    cmd ./autogen.sh

    mci
    mcb ./configure
    mcb   --host="$targethost"
    mcb   --with-sysroot="$crossbinrootdir"/sysroot
    mcg

    cmd make -j

    stack-pop AR
    stack-pop AS
    stack-pop CC
    stack-pop CFLAGS
    stack-pop CXX
    stack-pop LD
    stack-pop STRIP
    stack-pop LDFLAGS
    stack-pop NOCONFIGURE

    stack-pop-path
}

build-cairo () {
    stack-push-path

    fun safe-rm-dir-allow-absolute "$sourcesdir" "$cairodirstub"
    chd "$sourcesdir"
    cmd git clone "$cairoremote" "$cairodirstub"
    chd "$cairodirstub"
    cmd git reset --hard "$cairorevision"
    fun redirect-in "$patchdir"/patch0-cairo-mutex patch -p0

    stack-push-xport AR armv7-linux-androideabi-ar
    stack-push-xport AS armv7-linux-androideabi-clang
    stack-push-xport CC armv7-linux-androideabi-clang
    stack-push-xport CFLAGS -fPIC
    stack-push-xport CXX armv7-linux-androideabi-clang++
    stack-push-xport LD armv7-linux-androideabi-ld
    stack-push-xport STRIP armv7-linux-androideabi-strip
    # --- don't automatically run configure after autogen.
    stack-push-xport NOCONFIGURE yes
    stack-push-xport LDFLAGS ''

    xport-append-space LDFLAGS -L"$pixmandir"/pixman/.libs
    xport-append-space LDFLAGS -L"$png16dir"/.libs

    xport-append PATH "$crossbindir"

    cmd ./autogen.sh

    # --- important to disable pthreads.
    # --- gl(es) stuff is no by default, but we set it explicitly to no for
    # clarity.

    mci
    mcb ./configure
    mcb     --host=armv7-linux-androideabi
    mcb     --enable-pthread=no
    mcb     --enable-xlib=no
    mcb     --enable-gobject=no
    mcb     --enable-xcb=no
    mcb     --enable-fc=no # --- fontconfig
    mcb     --enable-ft=no # --- freetype
    mcb     --enable-gl=no
    mcb     --enable-glesv2=no
    mcb     --with-sysroot="$crossbinrootdir"/sysroot
    mcg

    # --- the test suite was failing to compile: hack to disable it.
    fun redirect-out test/Makefile echo 'all:'

    cmd make -j

    stack-pop AR
    stack-pop AS
    stack-pop CC
    stack-pop CFLAGS
    stack-pop CXX
    stack-pop LD
    stack-pop STRIP
    stack-pop LDFLAGS
    stack-pop NOCONFIGURE

    stack-pop-path
}

build-extra-sources () {
    fun build-sdl
    fun build-png16
    fun build-pixman
    fun build-cairo
}

init-android-build () {
    stack-push-path

    xport-prepend PATH "$sdkplattoolsdir"
    xport-prepend PATH "$sdktoolsdir"
    xport-prepend PATH "$ndkdir"

    # local android_home="${ANDROID_HOME:-}"
    # xport ANDROID_HOME "$sdkdir"

    xport BUILDSDL no
    xport CLEANBUILDDIR no

    # fun safe-rm-dir-allow-absolute "$builddir"

    mci
    mcb "$sdlandroidbuilddir"/androidbuild.sh
    mcb   "$builddir"
    mcb   "$sdldir"
    mcb   "android-$androidsdkapiversion"
    mcb   "$acat_plugin_appname"
    mcb   "${srcfiles[@]}"
    mcg

    # --- script copies folder, don't want xxx
    cmd rm -rf "$jnidir"/SDL

    mci
    mcb cp
    mcb   "$srcdir"/SDLActivity.java
    # --- path doesn't matter: all java files are picked up and bundled.
    mcb   "$builddir"/src/org/libsdl/app/SDLActivity.java
    mcg

    # xport ANDROID_HOME "$android_home"
    xport BUILDSDL
    xport CLEANBUILDDIR

    stack-pop-path
}

prepare-prebuilt-androidmk () {
    local modname="$1"
    local libso="$2"
	fun redirect-out Android.mk cat <<- EOT
	LOCAL_PATH := \$(call my-dir)

	include \$(CLEAR_VARS)
	LOCAL_MODULE := $2
	LOCAL_SRC_FILES := \$(TARGET_ARCH_ABI)/$1
	include \$(PREBUILT_SHARED_LIBRARY)
	EOT
}

prepare-prebuilt-static-androidmk () {
    local modname="$1"
    local liba="$2"
	fun redirect-out Android.mk cat <<- EOT
	LOCAL_PATH := \$(call my-dir)

	include \$(CLEAR_VARS)
	LOCAL_MODULE := $2
	LOCAL_SRC_FILES := \$(TARGET_ARCH_ABI)/$1
	include \$(PREBUILT_STATIC_LIBRARY)
	EOT
}

_prepare-prebuilt-sdl () {
    mkchd "$jnidir"/SDL2
    cmd cp -v "$sdlbuilddirsources"/"$sdlremotestub"/include/*.h .

    # --- experimental, try to access java methods from c.
    # --- some .h files are in src, not include
    # mkd include-src
    # cmd cp -v "$sdldir"/src/*.h include-src
    # mkd include-src/dynapi
    # cmd cp -v "$sdldir"/src/dynapi/*.h include-src/dynapi
    # mkd include-src/core/android
    # cmd cp -v "$sdldir"/src/core/android/SDL_android.h include-src/core/android

    fun prepare-prebuilt-androidmk libSDL2.so sdl2-prebuilt

    mkchd "$jnidir"/SDL2_image
    cmd cp -v "$sdlbuilddirsources"/"$sdlimageremotestub"/*.h .
    fun prepare-prebuilt-androidmk libSDL2_image.so sdl2-image-prebuilt

    mkchd "$jnidir"/SDL2_ttf
    cmd cp -v "$sdlbuilddirsources"/"$sdlttfremotestub"/*.h .
    fun prepare-prebuilt-androidmk libSDL2_ttf.so sdl2-ttf-prebuilt

    # --- .h and .so files for cairo will be copied during prepare-cairo-libs
    mkchd "$jnidir"/cairo
    fun prepare-prebuilt-androidmk libcairo.so cairo-prebuilt

    mkchd "$jnidir"/pixman
    fun prepare-prebuilt-androidmk libpixman-1.so pixman-prebuilt

    mkchd "$jnidir"/png16
    fun prepare-prebuilt-androidmk libpng16.so png16-prebuilt
}

prepare-cairo-libs () {
    local target=$1

    local cairotargetdir="$jnidir"/cairo/"$target"
    mkd "$cairotargetdir"
    # --- copy through symlink, might not be portable.
    cmd cp "$cairodir"/src/.libs/libcairo.so "$cairotargetdir"/libcairo.so

    cmd cp -a "$cairodir"/src/*.h "$jnidir"/cairo

    local pixmantargetdir="$jnidir"/pixman/"$target"
    mkd "$pixmantargetdir"
    cmd cp "$pixmandir"/pixman/.libs/libpixman-1.so "$pixmantargetdir"

    local png16targetdir="$jnidir"/png16/"$target"
    mkd "$png16targetdir"
    cmd cp "$png16dir"/.libs/libpng16.so "$png16targetdir"
}

_prepare-cairo-libs-arm () {
    fun prepare-cairo-libs "$target_arm"
}

# --- myhs and myhs-prebuilt are hardcoded, also in build-and-link-hs.
android-mk-main () {
    cat <<-EOF
	LOCAL_PATH := \$(call my-dir)

	include \$(CLEAR_VARS)

	LOCAL_MODULE := main

	LOCAL_CFLAGS := -std=c99

	# --- the ghc include is necessary for HsFFI.h
	LOCAL_C_INCLUDES := \$(LOCAL_PATH)/../SDL2 \
	                    \$(LOCAL_PATH)/../SDL2/include-src/core/android \
	                    \$(LOCAL_PATH)/../SDL2_image \
	                    \$(LOCAL_PATH)/../SDL2_ttf \
	                    \$(LOCAL_PATH)/../myhs \
	                    $ghclibpath/include

	# /media/usr//lib/armv7-linux-androideabi-ghc-8.3.20171120/include

	# \$(warning \$(LOCAL_C_INCLUDES))

	LOCAL_SRC_FILES := $cmain

	LOCAL_SHARED_LIBRARIES := sdl2-prebuilt sdl2-image-prebuilt sdl2-ttf-prebuilt cairo-prebuilt \
	    pixman-prebuilt png16-prebuilt \
	    myhs-prebuilt

	# --- should be only system libs
	LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -llog -landroid -lz

	include \$(BUILD_SHARED_LIBRARY)
	EOF
}

main-build-prepare-android-build () {
    fun redirect-in-cmd android-mk-main fun prepare-android-mk "$jnidir"
}

prepare-assets () {
    local ary=("$builddir" assets)
    fun safe-rm-dir-array-allow-absolute ary
    cmd cp -ar "$srcassetsdir" "$buildassetsdir"
}

prepare-text () {
    fun redirect-out "$builddir"/res/values/strings.xml cat <<- EOT
	<?xml version="1.0" encoding="utf-8"?>
	<resources>
	    <string name="app_name">"$acat_plugin_appnametext"</string>
	</resources>
	EOT
}

prepare-icon () {
    local icon="$srcassetsdir"/launcher/fish-orig.png
    local formats=(
        "mdpi 48"
        "hdpi 72"
        "xhdpi 96"
        "xxhdpi 144"
    )
    local i
    local format
    local sz
    local pf
    for i in "${formats[@]}"; do
        read format sz <<< "$i"
        printf -v pf "%sx%s" $sz $sz
        cmd convert "$icon" -resize "$pf" "$builddir"/res/drawable-"$format"/ic_launcher.png
    done
}

_prepare-sdl-pkg-config () {
	fun redirect-out "$sdlpkgconfig" cat <<- EOT
	Name: sdl2
	Description: sdl2 pkg-config stub
	Version: 2.0.7
	EOT
}

prepare-build-pre-hs () {
    fun _prepare-cairo-libs-arm
    fun _prepare-prebuilt-sdl
    fun _prepare-sdl-pkg-config
}

prepare-build () {
    fun init-android-build

# see dockerfile
# fun prepare-build-pre-hs

    fun main-build-prepare-android-build
    fun prepare-assets
    fun prepare-icon
    fun prepare-text
}

build-hs-packages () {
    local aryname="$1"
    local lines=()
    fun copy-array lines "$aryname"
    local type
    local pack
    local line

    for line in "${lines[@]}"; do
        read type pack <<< "$line"
        if [ "$type" = cabal ]; then
            fun cabal-install-cross-allow-newer "$pack"
        elif [ "$type" = cabal-no-allow-newer ]; then
            fun cabal-install-cross-no-allow-newer "$pack"
        elif [ "$type" = custom ]; then
            fun cabal-install-cross-custom-allow-newer "$pack"
        else
            error "Bad input: ^$type^ $pack"
        fi
    done
}

hs-build-deps-init () {
    local ver
    quiet-say
    ver=$(ghccmd-armeabi-v7a --numeric-version)
    noisy-say

    chd "$rootdir"

    local dep
    for dep in "${cabaldepsnormal[@]}"; do
        fun cabal-install-normal "$dep"
    done

    chd "$hsdir"

    # xxx array
    fun safe-rm-dir-allow-absolute "$hsdir" .cabal-sandbox
    cmd rm -f "$hsdir"/cabal.sandbox.config

    cmd cabal-it sandbox init

    # --- legacy: cabal should make it, but at some point we had to help
    # it.
    mkd "$packageconfd"

    cmd cabal-it update
}

# --- OpenGLRaw-3.3.0.1: works using debian armef packages, passed through #
# extra-include-dirs and extra-lib-dirs.
# --- note that we don't need to bundle the GL(ES)/(U) libs: they are
# already on the android.

hs-build-deps () {
    fun build-hs-packages haskellpackages_deps
}

hs-build-user () {
    fun build-hs-packages haskellpackages_plugin
}

# --- this is the main Haskell entry point (e.g. Hslib.hs)
build-and-link-hs-main () {
    mci
    mcb cmd ghccmd-armeabi-v7a --make

    mcb     -fPIC
    mcb     -O -j2
    mcb     -outputdir "$buildhsdirarmeabiv7a" -odir "$buildhsdirarmeabiv7a"
    mcb     -hidir "$buildhsdirarmeabiv7a" -stubdir "$buildhsdirarmeabiv7a"
    mcb     -i -i"$buildhsdirarmeabiv7a"
    mcb     -isrc
    mcb     -I"$buildhsdirarmeabiv7a" -optP-include
    # --- necessary?
    mcb     -this-unit-id hs-lib-0.1.0.0-J40A73rTosB5aPCr4h7OXN
    mcb     -package-db="$packageconfd"

    mcb     -XHaskell2010
    mcb     "$hsdir"/src/Hslib.hs
    mcg
}

get-static-lib-path () {
    local retaryname=$1
    local dir=$2
    local lib=$3
    local findargs=("$dir" -name "*$lib*.a")

    quiet-info
    find-count _count "${findargs[@]}"
    noisy-info

    # --- then we do the find a third time.

    local res
    if [ "$_count" = 0 ]; then
        error "Failed to find lib $lib in $dir"
    elif [ "$_count" -gt 1 ]; then
        warn "Several matches ($_count) for lib $lib in $dir: taking them all!"
    fi

    local res
    local i=1
    # while read -d $'\0' res; do
        # if [ "$i" = "$_count" ]; then break; fi
        # let i=i+1
    # done < <(find "${findargs[@]}" -print0)

    eval-it '%s=()' "$retaryname"
    while read -d $'\0' res; do
        eval-it '%s+=("$res")' "$retaryname"
    done < <(find "${findargs[@]}" -print0)

    # read -d '' "$ret" < <(find "${findargs[@]}") || true
    # read -d $'\0' "$ret" < <(find "${findargs[@]}" -print0) || true
    # read -d '' "$ret" <<< "$res" || true
}

package-static-libs () {
    local retaryname=$1; shift
    local dir=$1; shift
    local libs=("$@")

    local i
    local j

    local ret=()

    for i in "${libs[@]}"; do
        fun get-static-lib-path _ret0 "$dir" "$i"

        for j in "${_ret0[@]}"; do
            ret+=("addlib $j")
        done
    done
    fun copy-array "$retaryname" ret
}

# --- generate a huge static library (half a gig).
# --- will be reduced by about 80% during ndk-build.

build-and-link-hs-staticlib () {
    mkd "$ardir"

    local dotafile="$ardir"/Hslib.a
    local mrifile="$ardir"/libmyhs.mri
    local finalarchive="$ardir"/libmyhs.a
    cmd ar r "$dotafile" "$hsdirbuild"/armeabi-v7a/Hslib.o

    local sandboxlibslines=()

    fun package-static-libs _ret0 "$sandboxdir" "${haskellpackages_deps_names[@]}"
    sandboxlibslines+=("${_ret0[@]}")
    fun package-static-libs _ret0 "$sandboxdir" "${haskellpackages_plugin_names[@]}"
    sandboxlibslines+=("${_ret0[@]}")

    local ghclibslines=()
    fun package-static-libs ghclibslines "$ghclibpath" "${ghcwiredindeps[@]}"

    local manuallines=()
    local line
    for line in "${manualstaticarchives[@]}"; do
        manuallines+="addlib $line"
    done

	fun redirect-out "$mrifile" cat <<- EOT
	create $finalarchive

	addlib $dotafile

	addlib $libffidir/libffi.a
	addlib $libiconvdir/libiconv.a

	; %(for i in "%{sandboxlibslines[@]}"; do echo "%i"; done)
	; %(for i in "%{ghclibslines[@]}"; do echo "%i"; done)
	; %(for i in "%{manuallines[@]}"; do echo "%i"; done)

	$(join-out $'\n' sandboxlibslines)
	$(join-out $'\n' ghclibslines)
	$(join-out $'\n' manuallines)

	save
	end
	EOT

    fun redirect-in "$mrifile" ar M
    cpa "$finalarchive" "$buildhsdirarmeabiv7a"
}

build-and-link-hs () {
    cmd build-and-link-hs-main
    cmd build-and-link-hs-staticlib

    mkchd "$jnidir"/myhs
    cpa "$buildhsdirarmeabiv7a"/Hslib_stub.h .
    fun prepare-prebuilt-static-androidmk libmyhs.a myhs-prebuilt
}

# --- myhs is hardcoded, also in android-mk-main and build-and-link-hs.
install-libraries () {
    chd "$jnidir"

    local target
    for target in "${targets[@]}"; do
        mkchd "$jnidir"/SDL2/"$target"
        cmd cp -ar "$builtlibsdirsource"/"$target"/libSDL2.so .
        mkchd "$jnidir"/SDL2_image/"$target"
        cmd cp -ar "$builtlibsdirsource"/"$target"/libSDL2_image.so .
        mkchd "$jnidir"/SDL2_ttf/"$target"
        cmd cp -ar "$builtlibsdirsource"/"$target"/libSDL2_ttf.so .
        mkchd "$jnidir"/myhs/"$target"
        cmd cp -v "$buildhsdirarmeabiv7a"/libmyhs.a .
    done
}

ndk-build () {
    chd "$builddir"
    cmd "$ndkbuildcmd" -j
}

ant-and-deploy () {
    chd "$builddir"
    cmd ant debug install
}

main-build () {
    fun build-and-link-hs
    fun install-libraries
    cmd ndk-build
}

apk () {
    fun ant-and-deploy
}

go-mode () {
    mode="$1"

    if [ "$mode" = clean ]; then
         fun clean
    elif [ "$mode" = init-gpg ]; then
         fun init-gpg
    elif [ "$mode" = get-sdk ]; then
         fun get-sdk
    # elif [ "$mode" = get-ndk ]; then
         # fun get-ndk
    elif [ "$mode" = init-extra-sources ]; then
         fun init-extra-sources
    elif [ "$mode" = build-extra-sources ]; then
         fun build-extra-sources
    elif [ "$mode" = prepare-build-pre-hs ]; then
         fun prepare-build-pre-hs
    elif [ "$mode" = hs-build-deps-init ]; then
         fun hs-build-deps-init
    elif [ "$mode" = hs-build-deps ]; then
         fun hs-build-deps
    elif [ "$mode" = hs-build-user ]; then
         fun hs-build-user
    elif [ "$mode" = prepare-build ]; then
         fun prepare-build
    elif [ "$mode" = main-build ]; then
         fun main-build
    elif [ "$mode" = apk ]; then
         fun apk
    elif [ -z "$mode" ]; then
         error "Missing mode. $USAGE"
    else
        printf -v mod "$(bright-red "$mode")"
        warn "Unknown mode: $mod"
        error "$USAGE"
    fi
}

go () {
    local mode
    for mode in "$@"; do
        fun go-mode "$mode"
    done
}

fun go "$@"

exit

# --- building the cabal ecosystems is difficult.
# the pain points are:
# - two-setup Setup
# - need to spoof pkg-config for e.g. cairo bindings
# - packages require custom tweaks because our ghc version is too new.
# - why not just do them all the two-step custom way? because custom ones
# don't recursively pull in deps.
# bleeding-edge
#   specifically: Monoid is now a subclass of Semigroup
#   other thinsg ...

# --- the difficult situation is: a package like distributive-0.6 requires a
# custom setup, so it has to be in a custom block, and it works.
# but, free-5.1 depends on distributive, and you don't (easily) know that
# beforehand, so if free comes first, in a normal block, it will try to pull
# in distributive, not realising it needs a custom setup, and fail.

# --- (re)builds the Activity and the .apk.

# xport-prepend PATH "$ghcpath"
# xport-prepend PATH "$clangpath5_0"

cmd ant-and-deploy
