#include <stdbool.h>

#include <android/log.h>
#include <android/asset_manager.h>

// --- taking out these includes causes app to crash, why?
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

// why was this necesssary?
// causing compile errors, taking out
// #include "SDL_android.h"

// ------

#include <HsFFI.h>

// --- skipping __GLASGOW_HASKELL__ guards.
# include "Hslib_stub.h"

void android_info (char *str)
{
    // fish();
    __android_log_write (ANDROID_LOG_INFO, "fish", str);
}

void android_warn (char *str)
{
    __android_log_write (ANDROID_LOG_WARN, "fish", str);
}

void android_error (char *str)
{
    __android_log_write (ANDROID_LOG_ERROR, "fish", str);
}

// --- dead end: AAssetManager_fromJava(JNIEnv* env, jobject assetManager);
// don't know how to get JNIEnv handle (seems to need NativeActivity).
void assets ()
{
    android_info ("PING 1");
    // AAssetManager *mgr = AAssetManager_fromJava (...);
    AAssetManager *mgr; // --- will segfault
    android_info ("PING 2");
    AAssetDir *dir = AAssetManager_openDir (mgr, "name");
    android_info ("PING 3");
    if (dir == NULL) {
        android_error ("can't open dir");
        return;
    }
    const char *filename;
    bool stop = false;
    while (true) {
        android_info ("checking next");
        filename = AAssetDir_getNextFileName (dir);
        if (filename == NULL) break;
        size_t l = strlen (filename);
        long ll = l + 6 + 1;
        char *s = malloc (ll * sizeof (char));
        sprintf (s, "file: %s", filename);
        android_info (s);
    }
    AAssetDir_close (dir);
}

// --- gets munged to SDL_main; other functions keep their names.
int main (int argc, char *argv[])
{
    android_info ("starting");

    if (argc != 2) {
        android_error ("Bad usage (SDL_main)");
        return 1;
    }

    char *imageBase64 = argv[1];

    android_info ("hs_init");
    hs_init (NULL, NULL);

    android_info ("launchit");
    launchit (imageBase64);

    android_info ("hs_exit");
    hs_exit ();

    return 0;
}
